import { Component, OnInit } from '@angular/core';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { UserblockService } from './userblock.service';
import { Inject, Injectable } from '@angular/core';

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit {
    user: any;
        userDetails=
           {
r_user_name:undefined,
user_password: undefined,
user_type: undefined,
f_name: undefined,
emp_id: undefined,
department: undefined,
CURRENT_DATE: undefined,
user_id: undefined,
point_id: undefined,
point_code: undefined,
department_id: undefined,
organization_id: undefined,
r_point_name: undefined,
v_point_type_id: undefined,
v_designation_id: undefined,
v_designation_name: undefined,
V_country_id: undefined,
V_country_Name: undefined,
v_organization_name: undefined
        }

    constructor(public userblockService: UserblockService,
        @Inject(LOCAL_STORAGE)private storage: WebStorageService) {

        this.user = {
            picture: 'assets/img/user/01.jpg'
        };
    }

    ngOnInit() {
        this.getUserLocal();
    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

    getUserLocal(){
        // console.log("users ",this.storage.get('user_details');
            this.userDetails=this.storage.get('user_details');
    }

}
