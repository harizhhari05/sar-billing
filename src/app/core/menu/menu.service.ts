    import { Injectable } from '@angular/core';
    import { DatePipe } from '@angular/common';

@Injectable()
export class MenuService {

    menuItems: Array<any>;
    myDate = new Date();
    Date:any;


    constructor(private datePipe: DatePipe) {
        this.menuItems = [];
        this.Date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
    }

    addMenu(items: Array<{
        text: string,
        heading?: boolean,
        link?: string,     // internal route links
        elink?: string,    // used only for external links
        target?: string,   // anchor target="_blank|_self|_parent|_top|framename"
        icon?: string,
        alert?: string,
        hidden?:boolean,
        submenu?: Array<any>
    }>) {
        items.forEach((item) => {
            this.menuItems.push(item);
        });
    }

    getMenu() {
        return this.menuItems;
    }

}
