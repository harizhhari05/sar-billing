import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppComponent } from './app.component';
// import { ToastrModule } from 'ng6-toastr-notifications';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';
import { SharedModule } from './shared/shared.module';
import { RoutesModule } from './routes/routes.module';
import { ToastrModule } from 'ng6-toastr-notifications';
import { PopupBoxesModule } from 'ng6-popup-boxes';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { StorageServiceModule } from 'angular-webstorage-service';
import { DatePipe } from '@angular/common';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
// import { MatButtonModule } from '@angular/material/button';
// import { MatDialogModule } from '@angular/material/dialog';
// import { ServiceComponent} from './services/services.component'

// https://github.com/ocombe/ng2-translate/issues/218
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
            ],
    imports: [
        HttpClientModule,
        BrowserAnimationsModule, // required for ng2-tag-input
        CoreModule,
        StorageServiceModule,
        ToastrModule.forRoot(), // ToastrModule added
        PopupBoxesModule.forRoot(),
        LayoutModule,
        ToastrModule.forRoot(),
        SharedModule.forRoot(),
        RoutesModule,
        Ng2SearchPipeModule,
        FormsModule,
    //     MatButtonModule,
    // MatDialogModule,
        // DatePipe,
        ToastrModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }) 
    ],
    providers: [ToasterService,DatePipe],
    bootstrap: [AppComponent],

})
export class AppModule { }
