import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Constant } from '../constants/constant';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class GenerateBillServiceService {
url:any;
retry:any;
  constructor(private http: HttpClient) { }
    httpOptions={

  	headers:new HttpHeaders({
  		'content-type':'application/json'
  	})
  }
     get_branchs():Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/point_origin?id=").pipe(retry(2))
  }
  get_json():Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "air-freight-json").pipe(retry(2))
  }
       get_customer(org_id):Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/customer_by_origin?origin_id=" + org_id +"&customer_id=-1").pipe(retry(2))
  }
       get_payment():Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/payment_modes").pipe(retry(2))
  }
    getbill_report(datas):Observable<any>{
  	return this.http.get(Constant.CONSTANT_API + "read/bill_invoice_list?customer_id="+datas.customer
  	+"&from_date="+datas.from_date +"&to_date="+datas.to_date +"&payment_mode="
  	+datas.payment+ "&invoice_id=&invoice_no=").pipe(retry(2))
  }
   getbill_duty_report(datas):Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/duty_bill_invoice_list?customer_id="+datas.customer
    +"&from_date="+datas.from_date +"&to_date="+datas.to_date +"&payment_mode=CREDIT&invoice_id=&invoice_no=").pipe(retry(2))
  }
  getbill_clr_report(datas):Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/service_bill_invoice_list?customer_id="+datas.customer
    +"&from_date="+datas.from_date +"&to_date="+datas.to_date +"&payment_mode=CREDIT&invoice_id=&invoice_no=").pipe(retry(2))
  }
  getbill_service_report(datas):Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/service_bill_list?customer_id="+datas.customer
    +"&from_date="+datas.from_date +"&to_date="+datas.to_date +"&origin_id="+datas.branch).pipe(retry(2))
  }
  bill_gen(data):Observable<any>{
  return this.http.post(Constant.CONSTANT_API + 'generate/customer_bill',JSON.stringify(data),this.httpOptions).pipe(retry(2))
}
bill_gen_Air(data):Observable<any>{
  return this.http.post(Constant.CONSTANT_API + 'generate/customer_bill_air_freight',JSON.stringify(data),this.httpOptions).pipe(retry(2))
}
  duty_bill_gen(data):Observable<any>{
    return this.http.post(Constant.CONSTANT_API + 'generate/customer_duty_bill',JSON.stringify(data),this.httpOptions).pipe(retry(2))
  }
  clr_bill_gen(data):Observable<any>{
    return this.http.post(Constant.CONSTANT_API + 'generate/customer_service_bill',JSON.stringify(data),this.httpOptions).pipe(retry(2))
  }
  service_bill_gen(data):Observable<any>{
    return this.http.post(Constant.CONSTANT_API + 'update/service_bill_list',JSON.stringify(data),this.httpOptions).pipe(retry(2))
  }
    get_origin_code(id):Observable<any>{
      return this.http.get(Constant.CONSTANT_API + 'read/point_origin_code?id='+id).pipe(retry(2))
    }
}
