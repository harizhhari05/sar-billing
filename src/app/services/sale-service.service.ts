import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Constant } from '../constants/constant';
import 'rxjs/add/operator/catch';


@Injectable({
  providedIn: 'root'
})
export class SaleServiceService {
url:any;
retry:any;
  constructor(private http: HttpClient) { }
    httpOptions={

  	headers:new HttpHeaders({
  		'content-type':'application/json'
  	})
  }
     get_origin():Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/point_origin?id=").pipe(retry(2))
  }
    getsale_report(datas):Observable<any>{
  	return this.http.get(Constant.CONSTANT_API + "/without_sales_entry?origin=" +datas.origin
  	+"&destination="+"&from_date="+datas.from_date +"&to_date="+datas.to_date).pipe(retry(2))
  }
}
