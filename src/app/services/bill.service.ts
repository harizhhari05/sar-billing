import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import  { retry,catchError } from 'rxjs/operators';
import { Constant } from '../constants/constant';
@Injectable({
  providedIn: 'root'
})
export class BillService {

  constructor(private http: HttpClient ) { }
     httpOptions={
	headers:new HttpHeaders({
		'content-type':'application'
	})
}
getBranch():Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'read/point_origin?id=').pipe(retry(2))
}
get_payment():Observable<any>{
    return this.http.get(Constant.CONSTANT_API + "read/payment_modes").pipe(retry(2))
}
get_shipment_type():Observable<any>{
  return this.http.get(Constant.CONSTANT_API + "read/shipment_type").pipe(retry(2));
}
get_service():Observable<any>{
  return this.http.get(Constant.CONSTANT_API +"read/transit_type").pipe(retry(2));
}
Edit_Bill(data):Observable<any>{
  return this.http.get(Constant.CONSTANT_API +"bill_no_change?old="+data.old_no+"&new="+data.new_no).pipe(retry(2));
}
get_destination():Observable<any>{
  return this.http.get(Constant.CONSTANT_API +"read/delivery_locations").pipe(retry(2));
}
getCus(origin_client_id):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'read/customer_by_origin?origin_id='
  	+origin_client_id+'&customer_id=-1').pipe(retry(2))
}
getTab(datas):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'read/invoice_view_bill_list?origin_id='+
  	datas.origin_id+'&customer='+datas.origin_client_id+'&from_date='+
  	datas.from_date+'&to_date='+datas.to_date+'&type=INVOICE').pipe(retry(2))
}
getdutyBill(datas):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'read/invoice_duty_bill_list?origin_id='+
    datas.origin_id+'&customer='+datas.origin_client_id+'&from_date='+
    datas.from_date+'&to_date='+datas.to_date+'&type=DUTY').pipe(retry(2))
}
getclearanceBill(datas):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'read/invoice_service_bill_list?origin_id='+
    datas.origin_id+'&customer='+datas.origin_client_id+'&from_date='+
    datas.from_date+'&to_date='+datas.to_date+'&type=INVOICE').pipe(retry(2))
}
getPdf(datas2):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'download/generate_domestic_duty_bill?bill_no='
    +datas2.hawb_billingInvoice_no+'&customer_acc='+datas2.client_id+
    '&invoice_base=&current_date='+datas2.duty_bill_date+
    '&origin_branch_id=&print_logo='+datas2.remove_logo+'&print_address='+datas2.remove_address+'&paymentTypeDisplayName='
    +datas2.payment_mode).pipe(retry(2))
}
getAirPdf(datas2):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'download/generate_air_freight_bill?bill_no='
    +datas2.hawb_billingInvoice_no+'&customer_acc='+datas2.client_id+
    '&invoice_base=&current_date='+datas2.duty_bill_date+
    '&origin_branch_id=&print_logo='+datas2.remove_logo+'&print_address='+datas2.remove_address+'&paymentTypeDisplayName='
    +datas2.payment_mode).pipe(retry(2))
}
getPdfinvoice(datas2):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'download/generate_invoice_bill_with_service?bill_no='
    +datas2.hawb_billingInvoice_no+'&customer_acc='+datas2.client_id+
    '&invoice_base=&current_date='+datas2.billing_date+
    '&origin_branch_id=&print_logo='+datas2.remove_logo+'&print_address='+datas2.remove_address+'&paymentTypeDisplayName='
    +datas2.payment_mode).pipe(retry(2))
}
getPdfclearance(datas2):Observable<any> {
  return this.http.get(Constant.CONSTANT_API + 'download/generate_service_bill_with_clearance?bill_no='
    +datas2.hawb_billingInvoice_no+'&customer_acc='+datas2.client_id+
    '&invoice_base=&current_date='+datas2.billing_date+
    '&origin_branch_id=&print_logo='+datas2.remove_logo+'&print_address='+datas2.remove_address+'&paymentTypeDisplayName='
    +datas2.payment_mode).pipe(retry(2))
}
getPdf_viewlist(data):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'download/generate_domestic_bill?bill_no='
    +data.hawb_billingInvoice_no+'&customer_acc='+data.client_name+'&invoice_base=&current_date='
    +data.billing_date+'&origin_branch_id=&print_logo='+data.remove_logo+'&print_address='+data.remove_address+
    '&paymentTypeDisplayName=undefined')
}
getList(hawb_billingInvoice_no):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'read/invoice_bill_by_no?id=&bill_no='
    +hawb_billingInvoice_no+'&customer_id=&from_date=&to_date=').pipe(retry(2))
}
get_origin():Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'read/point').pipe(retry(2));
}
get_issue(data):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'invoice_issuing?starting_number='
    +data.starting_number+'&ending_number='+data.ending_number+'&origin='+data.point_id).pipe(retry(2));
}
get_bill_details(bill_no):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'read/invoice?invoice_no='+bill_no).pipe(retry(2));
}
delete_invoice(id):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'delete/invoice_bill?id='+id).pipe(retry(2));
}
Addto(id):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'read/bill_invoice_list?customer_id=&from_date=&to_date=&payment_mode=&invoice_id=&invoice_no='+id).pipe(retry(2));
}
add_sinle_bill(data):Observable<any>{
  return this.http.post(Constant.CONSTANT_API + 'generate/single_customer_bill',JSON.stringify(data),this.httpOptions).pipe(retry(2));
}
upadte_date(data):Observable<any>{
  return this.http.post(Constant.CONSTANT_API + 'update/generated_bill',JSON.stringify(data),this.httpOptions).pipe(retry(2));
}
delete_hawb_invoice(bill_no):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'delete/total_invoice_bill?bill_no='+bill_no).pipe(retry(2))
}
delete_hawb_duty(bill_no):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'delete/total_duty_bill?bill_no='+bill_no).pipe(retry(2))
}
delete_hawb_clearance(bill_no):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'delete/total_service_bill?bill_no='+bill_no).pipe(retry(2))
}
get_invoice_report(datas):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'billed_invoice_list?branch='+
    datas.origin_id+'&customer='+datas.origin_client_id+'&from_date='+
    datas.from_date+'&to_date='+datas.to_date )
}
get_month_report(datas):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'read/monthly_report?from_date='+datas.from_date
    +'&to_date='+datas.to_date );
}
get_duty_report(datas):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'billed_duty_list?branch='+
    datas.origin_id+'&customer='+datas.origin_client_id+'&from_date='+
    datas.from_date+'&to_date='+datas.to_date )
}
get_non_invoice_report(datas):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'non_billed_invoice?customer_id='+datas.origin_client_id+
    '&from_date=&to_date=&branch='+datas.origin_id+'&type=INVOICE' )
}
get_non_duty_report(datas):Observable<any>{
  return this.http.get(Constant.CONSTANT_API + 'non_billed_invoice?customer_id='+datas.origin_client_id+
    '&from_date=&to_date=&branch='+datas.origin_id+'&type=DUTY')
}
getLogin(data) : Observable<Object> {
    let url=Constant.CONSTANT_API+"api/admin_panel_login";

    return this.http.post(`${url}`,data);
  }
}
	