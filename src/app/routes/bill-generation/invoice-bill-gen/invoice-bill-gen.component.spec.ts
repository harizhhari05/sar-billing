import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceBillGenComponent } from './invoice-bill-gen.component';

describe('InvoiceBillGenComponent', () => {
  let component: InvoiceBillGenComponent;
  let fixture: ComponentFixture<InvoiceBillGenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceBillGenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceBillGenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
