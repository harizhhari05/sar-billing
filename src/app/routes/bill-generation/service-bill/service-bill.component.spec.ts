import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceBillComponent } from './service-bill.component';

describe('ServiceBillComponent', () => {
  let component: ServiceBillComponent;
  let fixture: ComponentFixture<ServiceBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
