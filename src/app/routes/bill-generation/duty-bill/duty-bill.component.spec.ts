import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DutyBillComponent } from './duty-bill.component';

describe('DutyBillComponent', () => {
  let component: DutyBillComponent;
  let fixture: ComponentFixture<DutyBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DutyBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DutyBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
