import { Component, OnInit } from '@angular/core';
import { GenerateBillServiceService } from "../../../services/generate-bill-service.service";
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import {ActivatedRoute,Router} from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-clearance-bill',
  templateUrl: './clearance-bill.component.html',
  styleUrls: ['./clearance-bill.component.scss']
})
export class ClearanceBillComponent implements OnInit {
branch:any;
customer:any;
payment:any;
from_date:any;
to_date:any;
datas:any;
branchlist:any;
customerlist:any;
paymentlist:any;
report=[];
frieght_charges:any;
for_test:any;
tot_f_charge:any;
fuel_s_charge:any;
c_g_s_t:any;
s_g_s_t:any;
i_g_s_t:any;
tot_net_amount:any;
extra_amt:any;
branch_code:any;
myDate = new Date();
Date:any;
invoice_date:any;
isChecked:any;
selected:any; 
report_ch:any=[];
invoce_no_edit:any;
insert_duty={
	Act_wgt:undefined,
	CGST: undefined,
	FSC_charge: undefined,
	IGST: undefined,
	InvoiceGenerateNumber:undefined,
	SGST: undefined,
	bcd: "",
	bill_amount: undefined,
	bill_of_entry_no: "",
	branchShortName: undefined,
	client_id: undefined,
	clr_igst: "",
	delivery_area: undefined,
	detention_charge: "",
	document_type_name: undefined,
	duty: undefined,
	extra_bill_amount: undefined,
	freight_charge: undefined,
	high_value: undefined,
	hundred_eou: undefined,
	ic_charges: undefined,
	invoice_date: undefined,
	invoice_generate_date: undefined,
	invoice_generate_type: undefined,
	invoice_id: undefined,
	invoice_no: undefined,
	mepz: undefined,
	narration_1: undefined,
	narration_2: undefined,
	narration_3: undefined,
	net_amount: undefined,
	origin_branch_id: undefined,
	origin_id: undefined,
	other_charges_1: undefined,
	other_charges_2: undefined,
	other_charges_3: undefined,
	payment_mode: undefined,
	rcmc_igst: undefined,
	sez: undefined,
	sws: "",
	total_amount: undefined,
	total_weight: undefined,
	transit_type_id: undefined,
}
temp = [];
rowsFilter = [];
insert_duty_data:any=[];
  constructor(public service:GenerateBillServiceService,public toastr: ToastrManager,
    private datePipe: DatePipe) {
this.Date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
   }

   checkall(isChecked){
     console.log("isChecked",isChecked);
     for (var i = 0; i < this.report.length; i++) {
       this.report[i].isChecked=isChecked;
     }
     isChecked="";
   }

generate_bill_dem(){
  // console.log("y",this.report[i].isChecked);
  this.report_ch=[];
   for (var i = 0; i < this.report.length; i++) {
     if(this.report[i].isChecked == true){
       this.report_ch.push(this.report[i]);
     }

   }
   console.log("loop",this.report_ch);
   if (this.report_ch.length == 0 || this.report_ch.length == null) {
     this.toastr.errorToastr('NO shipment selected','failed');
   }
   else{
   this.report_ch[0].origin_branch_id=this.branch;
   this.report_ch[0].branchShortName=this.branch_code;
   this.report_ch[0].InvoiceGenerateNumber="DM2021/"+this.branch_code;
  for (var i = 0; i < this.report_ch.length; i++) {
    this.report_ch[i].invoice_generate_date =this.invoice_date;
  }
  this.service.bill_gen(this.report_ch).subscribe(res=>{
    if (res['code']==200) {
      this.toastr.successToastr('Bill generated success', 'Success');
      this.submit();
    }
    else{
      this.toastr.errorToastr('Bill generated failed', 'failed');
    }
  })

}
}
updateFilter(event) {
   // this.searchh()
   // console.log(event)
        const val = event.target.value.toLowerCase();
// console.log(event.target.value)
        // filter our data

        const temp = this.report.filter(function(report) {
            return report.invoice_no.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rowsFilter = temp;
        // this.data2=this.rowsFilter;

        // this.table.offset = 0;
    }


edit(edit_row){
	// this.insert_duty.invoce_no=edit_row.invoce_no;
	this.insert_duty.Act_wgt=edit_row.total_weight;
	this.insert_duty.CGST=edit_row.CGST;
	this.insert_duty.FSC_charge=edit_row.FSC_charge;
	this.insert_duty.IGST=edit_row.IGST;
	this.insert_duty.SGST=edit_row.SGST;
	this.insert_duty.bill_amount=edit_row.net_amount;
	this.insert_duty.delivery_area=edit_row.delivery_area;
	this.insert_duty.document_type_name=edit_row.document_type_name;
	this.insert_duty.extra_bill_amount=edit_row.extra_bill_amount;
	this.insert_duty.freight_charge=edit_row.freight_charge;
	this.insert_duty.invoice_date=edit_row.invoice_date;
	this.insert_duty.invoice_id=edit_row.invoice_id;
	this.insert_duty.invoice_no=edit_row.invoice_no;
	this.insert_duty.net_amount=edit_row.net_amount;
	this.insert_duty.origin_id=edit_row.origin_id;
	this.insert_duty.payment_mode=edit_row.payment_mode;
	this.insert_duty.total_amount=edit_row.net_amount;
	this.insert_duty.total_weight=edit_row.total_weight;
	this.insert_duty.transit_type_id=edit_row.transit_type_id;
	this.insert_duty.invoice_generate_type="SERVICE";
	this.insert_duty.client_id=this.customer;
}
print(){}
save(){
	this.insert_duty.origin_branch_id=this.branch;
   this.insert_duty.branchShortName=this.branch_code;
   this.insert_duty.InvoiceGenerateNumber="CL2021/"+this.branch_code;
   this.insert_duty.invoice_generate_date =this.invoice_date;
   for (var i = 0; i < 1; i++) {
   	this.insert_duty_data[i]=this.insert_duty;
   }
   console.log("ins",this.insert_duty);
   this.service.clr_bill_gen(this.insert_duty_data).subscribe(res=>{
   	if (res['code']==200) {
   		this.toastr.successToastr('Bill generated success','success');
       this.reset_entry();
   	}
   	else{
   		this.toastr.errorToastr('Bill generated failed','failed');
   	}
   })
}

branch_list()
{
  this.service.get_branchs().subscribe(params=>{
  this.branchlist=params.data;
  console.log('branchlist', this.branchlist)
  });
}

customer_list()
{
  this.service.get_customer(this.branch).subscribe(params=>{
  this.customerlist=params['data'];
  console.log('customerlist', this.customerlist);
  this.get_code();
});
}
get_code(){
  this.service.get_origin_code(this.branch).subscribe(data=>{
    this.branch_code=data['data'][0]['origin_code'];
  })
}

payment_list()
{
  this.service.get_payment().subscribe(params=>{
  this.paymentlist=params.data;
  console.log('paymentlist', this.paymentlist)
  });
}

submit(){
	this.datas={
		branch:this.branch,
		customer:this.customer,
		payment:this.payment,
		from_date:this.from_date,
		to_date:this.to_date,
		invoice_date:this.invoice_date
	}
  console.log('cus', this.datas)
	  	this.service.getbill_clr_report(this.datas).subscribe(param=>{
  		this.report=param['data'];
      if (this.report.length !== 0) {
      this.rowsFilter=this.report;
      for (var i = 0; i < this.report.length; ++i) {
        this.report[i].isChecked=true;
      }
  		console.log("refe", this.report);
      this.frieght_charges=param['data']['length'];
      console.log("length",this.frieght_charges)
      this.tot_f_charge=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.tot_f_charge = param['data'][i]['freight_charge'] - this.tot_f_charge + this.tot_f_charge + this.tot_f_charge;
      }
      this.tot_f_charge=this.tot_f_charge.toFixed(2);
      this.fuel_s_charge=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.fuel_s_charge = param['data'][i]['FSC_charge'] - this.fuel_s_charge + this.fuel_s_charge + this.fuel_s_charge;
      }
      this.fuel_s_charge=this.fuel_s_charge.toFixed(2);
      // this.extra_amt=0;
      // for (var i = 0; i < this.frieght_charges; i++) {
      //   this.extra_amt = param['data'][i]['extra_bill_amount'] - this.extra_amt + this.extra_amt + this.extra_amt;
      // }
      // this.extra_amt=this.extra_amt.toFixed(2);
      this.c_g_s_t=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.c_g_s_t = param['data'][i]['CGST'] - this.c_g_s_t + this.c_g_s_t + this.c_g_s_t;
      }
      this.c_g_s_t=this.c_g_s_t.toFixed(2);
      this.s_g_s_t=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.s_g_s_t = param['data'][i]['SGST'] - this.s_g_s_t + this.s_g_s_t + this.s_g_s_t;
      }
      this.s_g_s_t=this.s_g_s_t.toFixed(2);
      this.i_g_s_t=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.i_g_s_t = param['data'][i]['IGST'] - this.i_g_s_t + this.i_g_s_t + this.i_g_s_t;
      }
      this.i_g_s_t=this.i_g_s_t.toFixed(2);
      this.tot_net_amount=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.tot_net_amount = param['data'][i]['net_amount'] - this.tot_net_amount + this.tot_net_amount + this.tot_net_amount;
      }
      this.tot_net_amount=this.tot_net_amount.toFixed(2);
  		}
      else{
        this.toastr.warningToastr('No bill found','Alert');
      }

  	});
}
reset()
{
	this.branch="",
	this.customer="",
	this.payment="",
	this.invoice_date=""
}
reset_entry(){
  this.insert_duty.duty="";
  this.insert_duty.high_value="";
  this.insert_duty.rcmc_igst="";
  this.insert_duty.hundred_eou="";
  this.insert_duty.sez="";
  this.insert_duty.mepz="";
  this.insert_duty.ic_charges="";
  this.insert_duty.narration_1="";
  this.insert_duty.narration_2="";
  this.insert_duty.narration_3="";
  this.insert_duty.other_charges_1="";
  this.insert_duty.other_charges_2="";
  this.insert_duty.other_charges_3="";

}
  ngOnInit() {
    this.isChecked=true;
    this.invoice_date=this.Date;
    this.from_date=this.Date;
    this.to_date=this.Date;
  	this.branch_list();
  	// this.customer_list();
  	this.payment_list();
  	// this.reset();
  }

}
