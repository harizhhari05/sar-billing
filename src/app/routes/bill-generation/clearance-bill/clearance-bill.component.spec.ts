import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearanceBillComponent } from './clearance-bill.component';

describe('ClearanceBillComponent', () => {
  let component: ClearanceBillComponent;
  let fixture: ComponentFixture<ClearanceBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearanceBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearanceBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
