import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirFightComponent } from './air-fight.component';

describe('AirFightComponent', () => {
  let component: AirFightComponent;
  let fixture: ComponentFixture<AirFightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirFightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirFightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
