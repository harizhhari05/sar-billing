import { Component, OnInit } from '@angular/core';
import { GenerateBillServiceService } from "../../../services/generate-bill-service.service";
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import {ActivatedRoute,Router} from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-air-fight',
  templateUrl: './air-fight.component.html',
  styleUrls: ['./air-fight.component.scss']
})
export class AirFightComponent implements OnInit {

  branch:any;
customer:any;
payment:any;
from_date:any;
to_date:any;
btn3:boolean;
datas:any;
branchlist:any;
customerlist:any;
paymentlist:any;
report=[];
frieght_charges:any;
for_test:any;
tot_f_charge:any;
fuel_s_charge:any;
c_g_s_t:any;
s_g_s_t:any;
i_g_s_t:any;
tot_net_amount:any;
extra_amt:any;
branch_code:any;
myDate = new Date();
Date:any;
invoice_date:any;
isChecked:any;
selected:any; 
report_ch:any=[];
print_invoice:any;
temp = [];
rowsFilter = [];
report_data=[];
invoice_no:any;
  constructor(public service:GenerateBillServiceService,public toastr: ToastrManager,
    private datePipe: DatePipe) {
this.Date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
   }

   checkall(isChecked){
     console.log("isChecked",isChecked);
     for (var i = 0; i < this.report.length; i++) {
       this.report[i].isChecked=isChecked;
     }
     for (var i = 0; i < this.report.length; i++) {
       this.report[i].tax=false;
     }
     for (var i = 0; i < this.report.length; i++) {
       this.report[i].non_tax=false;
     }
     isChecked="";
   }

generate_bill(){
  // console.log("y",this.report[i].isChecked);
  this.report_ch=[];
   for (var i = 0; i < this.report.length; i++) {
     if(this.report[i].isChecked == true){
       this.report_ch.push(this.report[i]);
     }

   }
   console.log("loop",this.report_ch);
   if (this.report_ch.length == 0 || this.report_ch.length == null) {
     this.toastr.errorToastr('NO shipment selected','failed');
   }
   else{
   this.report_ch[0].origin_branch_id=this.branch;
   this.report_ch[0].branchShortName=this.branch_code;
   this.report_ch[0].InvoiceGenerateNumber="IN2021/"+this.branch_code;
  for (var i = 0; i < this.report_ch.length; i++) {
    this.report_ch[i].invoice_generate_date =this.invoice_date;
    this.report_ch[i].invoice_generate_type ="INVOICE";
  }
  this.service.bill_gen(this.report_ch).subscribe(res=>{
    if (res['code']==200) {
    	this.print_invoice=res['data'];
      this.toastr.successToastr('Bill generated success', 'Success');
      this.submit();
    }
    else{
      this.toastr.errorToastr('Bill generated failed', 'failed');
    }
  })

}
}
edit(data){
	this.invoice_no=data.invoice_no;
}
updateFilter(event) {
        const val = event.target.value.toLowerCase();
        const temp = this.report_data.filter(function(report_data) {
            return report_data.invoice_no.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rowsFilter = temp;
    }

branch_list()
{
  this.service.get_branchs().subscribe(params=>{
  this.branchlist=params.data;
  console.log('branchlist', this.branchlist)
  });
}
get_Json()
{
  this.service.get_json().subscribe(params=>{
  this.report=params;
  for (var i = 0; i < 22; ++i) {
          this.report[i].isChecked=true;
        }
  console.log('Json', this.report)
  });
}

customer_list()
{
  this.service.get_customer(this.branch).subscribe(params=>{
  this.customerlist=params['data'];
  console.log('customerlist', this.customerlist);
  this.get_code();
});
}
get_code(){
  this.service.get_origin_code(this.branch).subscribe(data=>{
    this.branch_code=data['data'][0]['origin_code'];
  })
}

payment_list()
{
  this.service.get_payment().subscribe(params=>{
  this.paymentlist=params.data;
  console.log('paymentlist', this.paymentlist)
  });
}

print(){
	
}

save(){
	this.report_ch=[];
   for (var i = 0; i < this.report.length; i++) {
     if(this.report[i].isChecked == true){
       this.report_ch.push(this.report[i]);
     }

   }
   if (this.report_ch.length == 0 || this.report_ch.length == null) {
     this.toastr.errorToastr('NO shipment selected','failed');
   }
   else{
   this.report_ch[0].origin_branch_id=this.branch;
   // this.report_ch[0].branchShortName=this.branch_code;
   // this.report_ch[0].InvoiceGenerateNumber="IN2021/"+this.branch_code;
  for (var i = 0; i < this.report_ch.length; i++) {
  	if (this.report_ch[i].tax == true) {
  		this.report_ch[i].taxable = 'Y';
  	}
  	else{
  		this.report_ch[i].taxable = 'N';
  	}
    this.report_ch[i].invoice_No =this.invoice_no;
    this.report_ch[i].customer =this.customer;
  }
     console.log("loop",this.report_ch);

  this.service.bill_gen_Air(this.report_ch).subscribe(res=>{
    if (res['code']==200) {
    	this.print_invoice=res['data'];
      this.toastr.successToastr('Bill generated success', 'Success');
      this.submit();
    }
    else{
      this.toastr.errorToastr('Bill generated failed', 'failed');
    }
  })

}
}

submit(){
	this.datas={
		branch:this.branch,
		customer:this.customer,
		payment:'CREDIT',
		from_date:this.from_date,
		to_date:this.to_date,
		invoice_date:this.invoice_date
	}
	this.btn3=true;
  console.log('cus', this.datas)
	  	this.service.getbill_report(this.datas).subscribe(param=>{
        this.report_data=param['data'];
        if (this.report_data.length !== 0) {
        this.rowsFilter=this.report_data;
        this.frieght_charges=param['data']['length'];
      }
      else{
        this.toastr.warningToastr('No bill Found','Alert')
      }
      this.btn3=false;
    	});
}
reset()
{
	this.branch="",
	this.customer="",
	this.payment="",
	this.from_date="",
	this.to_date="",
	this.invoice_date=""
}
  ngOnInit() {
    this.isChecked=true;
    this.btn3=true;
    this.get_Json();
    this.invoice_date=this.Date;
    this.from_date=this.Date;
    this.to_date=this.Date;
  	this.branch_list();
  	// this.customer_list();
  	this.payment_list();
  	// this.reset();
  }

}