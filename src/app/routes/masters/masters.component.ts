import { Component, OnInit,ViewChild,ElementRef, } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ToastrManager } from 'ng6-toastr-notifications';
import { PopupManager } from 'ng6-popup-boxes';
import { MasterServiceService } from '../../services/master-service.service';
const swal = require('sweetalert');
import * as XLSX from 'ts-xlsx';


@Component({
  selector: 'app-masters',
  templateUrl: './masters.component.html',
  styleUrls: ['./masters.component.scss']
})
export class MastersComponent implements OnInit {
  @ViewChild("inputBox") _el: ElementRef;
  @ViewChild("myInput") myInput: ElementRef;

	branchlist:any;
	customerlist:any;
  id:any;
	tarifflist:any;
	origin_id_by_customer:any;
	customer_id:any;
	serviceList:any;
	paymentList:any;
	shipmentList:any;
  rateTypeList:any;
  fileURL:any;
  rateListEdit={
    customer_rate_id: undefined,
    customer_name: undefined,
    destination_name: undefined,
    service_type: undefined,
    shipment_type: undefined,
    minimum_wgt: undefined,
    maximum_wgt: undefined,
    min_rate_per_shipment: undefined,
    transit_days_to_delivery: undefined,
    fuel_sur_charge: undefined,
    flat_rate_per_kg_above_maximum: undefined,
    rate_calculation_method: undefined,
    wh_storage_location_name: undefined,
    transit_type_name: undefined,
    document_type_name: undefined,
    payment_mode: undefined,
    client_name: undefined,
    client_code: undefined,
    clr_chrg_high_duty:undefined,
    delivery_charge:undefined,
    pickup_charge:undefined,
    clr_chrg_ic_duty: undefined,
    origin_country: undefined,
    origin_branch: undefined,
    destination_country:undefined,
    origin_country_name:undefined,

}
  insertList:any;
  insertSlap={
    customer_rate_master:undefined,
      slap_weight:undefined,
      slap_amount:undefined,
      slap_rate_type_UOM:undefined
  }
  updateList={
    customer_rate_id:undefined,
      customer_name:undefined,
      destination_name:undefined,
      service_type:undefined,
      shipment_type:undefined,
      payment_mode:undefined,
      minimum_wgt:undefined,
      maximum_wgt:undefined,
      min_rate_per_shipment:undefined,
      transit_days_to_delivery:undefined,
      fuel_sur_charge:undefined,
      flat_rate_per_kg_above_maximum:undefined,
      rate_calculation_method:undefined,
      wh_storage_location_name:undefined,
      transit_type_name:undefined,
      document_type_name:undefined,
      client_name:undefined,
      client_code:undefined,
      clr_chrg_high_duty:undefined,
      delivery_charge:undefined,
      pickup_charge:undefined,
      clr_chrg_ic_duty: undefined,
      origin_country: undefined,
      origin_branch: undefined,
      destination_country:undefined,
      origin_country_name:undefined,
  }
	rateList={
		customer_name:"",
		wh_storage_location_name:"",
		payment_mode:"",
		transit_type_name:"",
		shipment_type:"",
		minimum_wgt:"",
		min_rate_per_shipment:"",
		transit_days_to_delivery:"",
		fuel_sur_charge:"",
    delivery_charge: "",
    pickup_charge: "",
    clr_chrg_high_duty: "",
    clr_chrg_ic_duty: "",
    origin_country: "",
    origin_branch: "",
    destination_country:"",
    // country_id:"",
    customer_rate_id: "",
    destination_name: "",
    service_type: "",
    maximum_wgt: "",
    flat_rate_per_kg_above_maximum: "",
    rate_calculation_method: "",
    document_type_name: "",
    client_name: "",
    client_code: "",
    origin_country_name:"",
    // country:""

	}
  slapListget:any;
  slapList={
    customer_slap_rate_id:undefined,
    customer_rate_master:undefined,
    slap_weight:undefined,
    slap_amount:undefined,
    slap_rate_type_UOM:undefined
  }
  slapUpdate={
    customer_slap_rate_id: undefined,
    customer_rate_master: undefined,
    slap_weight: undefined,
    slap_amount: undefined,
    slap_rate_type_UOM: undefined
  }
	destinationList:any;
  btn:any;
  btn_add:any;
  btn_slp:any;
  btn_slp_add:any;
  master_id:any;
  brach_countryList:any;
  Country_list:any;
  deliveryList:any;
arrayBuffer:any;
    hawbArray:any;
    excelLength : any;
    file:File;
  constructor(public http:HttpClientModule, public service:MasterServiceService,
   public toastr: ToastrManager, public popupManager: PopupManager) { }

  branchList(){
  	this.service.get_branch().subscribe(branch=>{
  		this.branchlist=branch['data'];
  		console.log("or",this.branchlist);
  	})
  }
  branchlist_by_country(country){
    this.service.get_branch_by_country(country).subscribe(list=>{
      this.brach_countryList=list['data'];
    })
  }
  get_delivery_location(){
    this.service.get_delivery_location().subscribe(list=>{
      this.deliveryList=list['data'];
    })
  }
  getCountry(){
    this.service.get_country().subscribe(list=>{
      this.Country_list=list['data'];
    })
  }
  customerList(origin_id_by_customer){
  	this.service.get_customer(origin_id_by_customer).subscribe(customer=>{
  		this.customerlist=customer['data'];
  	})
  }
  tariffList(customer_id){
    this.btn_slp_add='2';
    this.btn_add='1';
    this.rateList.customer_name=customer_id;
  	this.service.get_tariff(customer_id).subscribe(tariff=>{
  		this.tarifflist=tariff['data'];
      console.log("tariff",this.tarifflist);
      this.reset_slap();
      this.slap_edit_master(this.id);
      this.reset();
  	})
  }
  tariffList_1(){
    this.btn_slp_add='2';
    this.btn_add='1';
    this.rateList.customer_name=this.customer_id;
    this.service.get_tariff(this.customer_id).subscribe(tariff=>{
      this.tarifflist=tariff['data'];
      console.log("tariff",this.tarifflist);
      this.reset_slap();
      this.slap_edit_master(this.id);
    })
    
  }
  destination(){
  	this.service.get_destination().subscribe(des=>{
  		this.destinationList=des['data'];
  	})
  }
  getService(){
  	this.service.get_service().subscribe(service=>{
  		this.serviceList=service['data'];
  	})
  }
  getPayment(){
  	this.service.get_payment().subscribe(payment=>{
  		this.paymentList=payment['data'];
  	})
  }
  getShipment(){
  	this.service.get_shipment_type().subscribe(shipment=>{
  		this.shipmentList=shipment['data'];
  	})
  }
  edit(id){
    this.btn='1';
    this.btn_add='2';
  	this.service.get_rate_id(id).subscribe(rate=>{
  		this.rateListEdit=rate['data'][0];

  		console.log("rateListEdit ",this.rateListEdit);
      this.insertSlap.customer_rate_master=rate['data'][0]['customer_rate_id'];
      this.rateList.customer_name=this.rateListEdit.customer_name;
      this.rateList.wh_storage_location_name=this.rateListEdit.destination_name;
      this.rateList.payment_mode=this.rateListEdit.payment_mode;
      this.rateList.transit_type_name=this.rateListEdit.service_type;
      this.rateList.shipment_type=this.rateListEdit.shipment_type;
      this.rateList.minimum_wgt=this.rateListEdit.minimum_wgt;
      this.rateList.min_rate_per_shipment=this.rateListEdit.min_rate_per_shipment;
      this.rateList.transit_days_to_delivery=this.rateListEdit.transit_days_to_delivery;
      this.rateList.fuel_sur_charge=this.rateListEdit.fuel_sur_charge;
      this.rateList.clr_chrg_high_duty=this.rateListEdit.clr_chrg_high_duty;
      this.rateList.clr_chrg_ic_duty=this.rateListEdit.clr_chrg_ic_duty;
      this.rateList.delivery_charge=this.rateListEdit.delivery_charge;
      this.rateList.pickup_charge=this.rateListEdit.pickup_charge;
      this.rateList.origin_country=this.rateListEdit.origin_country;
      if (this.rateListEdit.origin_branch==null || this.rateListEdit.origin_branch == "") {
        this.rateList.origin_branch="";
      }
      else{
        this.branchlist_by_country(this.rateList.origin_country);
              this.rateList.origin_branch=this.rateListEdit.origin_branch;
}
      // this.rateList.origin_branch=this.rateListEdit.origin_branch;
      
      this.rateList.destination_country=this.rateListEdit.destination_country;
      this.updateList.customer_rate_id=this.rateListEdit.customer_rate_id;
      this.updateList.maximum_wgt=this.rateListEdit.maximum_wgt;
      this.updateList.flat_rate_per_kg_above_maximum=this.rateListEdit.flat_rate_per_kg_above_maximum;
      this.updateList.rate_calculation_method=this.rateListEdit.rate_calculation_method;
      this.updateList.wh_storage_location_name=this.rateListEdit.wh_storage_location_name;
      this.updateList.document_type_name=this.rateListEdit.document_type_name;
      this.updateList.client_name=this.rateListEdit.client_name;
      this.updateList.client_code=this.rateListEdit.client_code;
      this.updateList.transit_type_name=this.rateListEdit.transit_type_name;
      
      // this.rateList.country=this.brach_countryList.country;
      
      
      
  	})
    // this.edit(id);
  }

  slap_edit_master(master_id){
    this.service.get_slap_id(master_id).subscribe(slap=>{
      this.slapListget=slap['data'];
      console.log("slapListget ",this.slapListget);
    })
  }
  edit_slap(id){
    this.btn_slp='1';
    this.btn_slp_add='1';
    this.service.get_slap_id_get(id).subscribe(slap_get=>{
      this.slapList=slap_get['data'][0];
      console.log("slap Edit slapList",this.slapList)
    })
  }
  getRate(){
    this.service.get_rate().subscribe(rate=>{
      this.rateTypeList=rate['data'];
    })
  }
  insert(){
    this.insertList={
      customer_name:this.rateList.customer_name,
      destination_name:this.rateList.wh_storage_location_name,
      service_type:this.rateList.transit_type_name,
      shipment_type:this.rateList.shipment_type,
      payment_mode:this.rateList.payment_mode,
      minimum_wgt:this.rateList.minimum_wgt,
      min_rate_per_shipment:this.rateList.min_rate_per_shipment,
      transit_days_to_delivery:this.rateList.transit_days_to_delivery,
      fuel_sur_charge:this.rateList.fuel_sur_charge,
      client_code:this.rateListEdit.client_code,
      client_name:this.rateListEdit.client_name,
      clr_chrg_high_duty:this.rateList.clr_chrg_high_duty,
      clr_chrg_ic_duty:this.rateList.clr_chrg_ic_duty,
      customer_rate_id:this.rateListEdit.customer_rate_id,
      delivery_charge:this.rateList.delivery_charge,
      origin_branch:this.rateList.origin_branch,
      flat_rate_per_kg_above_maximum: this.rateListEdit.flat_rate_per_kg_above_maximum,
      maximum_wgt:this.rateListEdit.maximum_wgt,
      destination_country:this.rateList.destination_country,
      origin_country:this.rateList.origin_country,
      origin_country_name:this.rateList.origin_country_name,
      pickup_charge:this.rateList.pickup_charge,
      rate_calculation_method:this.rateListEdit.rate_calculation_method,
      wh_storage_location_name:this.rateList.wh_storage_location_name,

  }
    console.log("ins",this.insertList);
    this.service.insert(this.insertList).subscribe(ins_res=>{
      if (ins_res['CODE'] == '200') {
        this.tariffList_1();
        this.toastr.successToastr('insert success','success');
        this.rateListEdit.customer_rate_id=ins_res['LASTINSERTID'];
        this.slap_edit_master(this.rateListEdit.customer_rate_id);
        // this.reset();
      }
      else if (ins_res['CODE']=='500') {
        this.toastr.errorToastr('insert Failed','Failed')
      }
      else{
        this.toastr.errorToastr('Failed','error');
      }
    })
  }
  update(){

      this.updateList.customer_rate_id=this.rateListEdit.customer_rate_id;
      this.updateList.customer_name=this.rateList.customer_name;
      this.updateList.destination_name=this.rateList.wh_storage_location_name;
      this.updateList.service_type=this.rateList.transit_type_name;
      this.updateList.shipment_type=this.rateList.shipment_type;
      this.updateList.payment_mode=this.rateList.payment_mode;
      this.updateList.minimum_wgt=this.rateList.minimum_wgt;

      this.updateList.clr_chrg_high_duty=this.rateListEdit.clr_chrg_high_duty;
      this.updateList.delivery_charge=this.rateList.delivery_charge;
      this.updateList.pickup_charge=this.rateList.pickup_charge;
      this.updateList.clr_chrg_ic_duty=this.rateList.clr_chrg_ic_duty;
      this.updateList.origin_country=this.rateList.origin_country;
      if (this.rateList.origin_branch == undefined || this.rateList.origin_branch == null) {
        this.updateList.origin_branch=this.rateListEdit.origin_branch;
      }
      else{
        this.updateList.origin_branch=this.rateList.origin_branch;
      }
      this.updateList.destination_country=this.rateList.destination_country;
      this.updateList.origin_country_name=this.rateList.origin_country_name;


      this.updateList.maximum_wgt=this.rateListEdit.maximum_wgt;
      this.updateList.min_rate_per_shipment=this.rateList.min_rate_per_shipment;
      this.updateList.transit_days_to_delivery=this.rateList.transit_days_to_delivery;
      this.updateList.fuel_sur_charge=this.rateList.fuel_sur_charge;
      this.updateList.flat_rate_per_kg_above_maximum=this.rateListEdit.flat_rate_per_kg_above_maximum;
      this.updateList.rate_calculation_method=this.rateListEdit.rate_calculation_method;
      this.updateList.wh_storage_location_name=this.rateListEdit.wh_storage_location_name;
      this.updateList.transit_type_name=this.rateListEdit.transit_type_name;
      this.updateList.document_type_name=this.rateListEdit.document_type_name;
      this.updateList.client_name=this.rateListEdit.client_name;
      this.updateList.client_code=this.rateListEdit.client_code;
    this.service.update(this.updateList).subscribe(res_update=>{
      if (res_update['CODE']=='200') {
        this.toastr.successToastr('update sucess', 'success');
        this.tariffList_1();
        this.btn='2';
        this.reset();
      }
      else if(res_update['CODE']=='500'){
        this.toastr.errorToastr('Update Failed','Failed')
      }
      else{
        this.toastr.errorToastr('Failed','error')
      }
    })
    
  }
  insert_slap(){
    console.log(" insert this.rateListEdit ",this.rateListEdit);
    this.insertSlap.customer_rate_master=this.rateListEdit.customer_rate_id;
    this.insertSlap.slap_weight=this.slapList.slap_weight;
    this.insertSlap.slap_amount=this.slapList.slap_amount;
    this.insertSlap.slap_rate_type_UOM=this.slapList.slap_rate_type_UOM;
    console.log("salp",this.insertSlap);
    this.service.insert_slap(this.insertSlap).subscribe(res_slap=>{
      if (res_slap['CODE']=='200') {
        this.toastr.successToastr('Insert success','SUCCESS');
        this.slap_edit_master(this.rateListEdit.customer_rate_id);
        this.reset_slap();
      }
      else{
        this.toastr.errorToastr('Failed','error')
      }
    })
    this._el.nativeElement.focus();
  }
  update_slap(){
    this.slapUpdate.customer_slap_rate_id=this.slapList.customer_slap_rate_id;
    this.slapUpdate.customer_rate_master=this.slapList.customer_rate_master;
    this.slapUpdate.slap_weight=this.slapList.slap_weight;
    this.slapUpdate.slap_amount=this.slapList.slap_amount;
    this.slapUpdate.slap_rate_type_UOM=this.slapList.slap_rate_type_UOM;
    this.service.update_slap(this.slapUpdate).subscribe(res_update=>{
      console.log("update_slap",this.slapUpdate);
      if (res_update['CODE'] =='200') {
        this.toastr.successToastr('update Success','SUCCESS');
        this.edit(this.rateListEdit.customer_rate_id);
        this.slap_edit_master(this.rateListEdit.customer_rate_id);
        this.reset_slap();
      }
      else{
        this.toastr.errorToastr('Failed','error')
      }
    })
    this.btn_slp='2';
    this.btn_slp_add='2';
    this._el.nativeElement.focus();
  }
  deleteTariff(id){
    this.popupManager.open('Delete', 'Do you really want to this item?', 
      {
            width: '300px',
            closeOnOverlay: false,
            animate: 'scale',
            actionButtons: 
            [
              {
                text: 'Yes',
                buttonClasses: 'btn-ok',
                onAction: () =>
                {
                this.service.delete_tariff(id).subscribe(res_del=>{
                  if (res_del[0]['CODE']=='200') {
                    this.tariffList_1();
                    this.toastr.successToastr('Delete Success','Deleted');
                  }
                })
                return true;
                }
              },
              {
                text: 'No',
                buttonClasses: 'btn-cancel',
                onAction: () => {
                  return false;
                }
              }
            ],
          
          });
  }
  print(){
    this.service.print(this.customer_id).subscribe(res_url=>{
      this.fileURL = res_url['file_url'];
      window.open(this.fileURL, '_blank');
    })
  }
  reset(){
    // this.rateList.customer_name=undefined;
    this.rateList.payment_mode=undefined;
    this.rateList.wh_storage_location_name=undefined;
    this.rateList.transit_type_name=undefined;
    this.rateList.shipment_type=undefined;
    this.rateList.minimum_wgt=undefined;
    this.rateList.min_rate_per_shipment=undefined;
    this.rateList.transit_days_to_delivery=undefined;
    this.rateList.fuel_sur_charge=undefined;
    this.rateList.clr_chrg_ic_duty=undefined;
    this.rateList.clr_chrg_high_duty=undefined,
    this.rateList.pickup_charge=undefined;
    this.rateList.delivery_charge=undefined;
    this.rateList.origin_branch=undefined;
    this.rateList.destination_country=undefined;
    this.rateList.origin_country=undefined;
    // this.rateList.Currency=undefined;
    // this.rateListEdit.origin_branch=undefined;
    // this.insertList.origin_branch=undefined;
    // this.updateList.origin_branch=undefined;
    this.rateList.origin_branch="";
    // this.rateList.country_id=undefined;
    // this.brach_countryList.origin_id=undefined;
    this.btn_add='1';
    this.btn='2';
  }
  reset_slap(){
    this.slapList.slap_weight=undefined,
    this.slapList.slap_amount=undefined,
    this.slapList.slap_rate_type_UOM=undefined
  }
  delete_slap(id){
    this.popupManager.open('Delete', 'Do you really want to this item?', 
      {
            width: '300px',
            closeOnOverlay: false,
            animate: 'scale',
            actionButtons: 
            [
              {
                text: 'Yes',
                buttonClasses: 'btn-ok',
                onAction: () =>
                {
                this.service.delete_slap(id).subscribe(res_del=>{
                  if (res_del[0]['CODE']=='200') {
                    this.toastr.successToastr('Delete Success','Success');
                    this.slap_edit_master(this.rateListEdit.customer_rate_id);
                  }
                })
                return true;
                }
              },
              {
                text: 'No',
                buttonClasses: 'btn-cancel',
                onAction: () => {
                  return false;
                }
              }
            ],
          
          });
  }
  delete_slp(id) {
        swal({
            title: 'Are you sure?',
            icon: 'warning',
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Yes, Deleted!',
                    value: true,
                    visible: true,
                    className: "bg-danger",
                    closeModal: true
                }
            }
        }).then((isConfirm) => {
            if (isConfirm) {
              this.service.delete_slap(id).subscribe(res_del=>{
                  if (res_del[0]['CODE']=='200') {
                    this.toastr.successToastr('Delete Success','Success');
                    this.edit(this.rateListEdit.customer_rate_id);
                  }
                })
            }
        });
    }

    deleteTarif(id) {
        swal({
            title: 'Are you sure?',
            icon: 'warning',
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Yes, Deleted!',
                    value: true,
                    visible: true,
                    className: "bg-danger",
                    closeModal: true
                }
            }
        }).then((isConfirm) => {
            if (isConfirm) {
              this.service.delete_tariff(id).subscribe(res_del=>{
                  if (res_del[0]['CODE']=='200') {
                    this.tariffList(this.customer_id);
                    this.toastr.successToastr('Delete Success','Deleted');
                  }
                })
            }
        });
    }
incomingfile(event : any) 
  {
    this.excelLength=0;
    this.file= event.target.files[0]; 
    if(this.file){
      this.Upload();
     }
  }
    Upload() {
    // this.hawbArray=[];
      let fileReader = new FileReader();
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            var data = new Uint8Array(this.arrayBuffer);
            var arr = new Array();
            for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, {type:"binary"});
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            this.hawbArray=XLSX.utils.sheet_to_json(worksheet,{raw:true});
            console.log("regular array ",this.hawbArray);
            this.excelLength=this.hawbArray.length;
            if(this.excelLength>0){
              this.toastr.successToastr("success","Excel","Valid Excel");
            }else {
              this.toastr.errorToastr("error","Excel","Invalid Excel Or Corrupted File.");
            }
        }
        fileReader.readAsArrayBuffer(this.file);
  }

   uploadHawbDetail(subjectcontents_or_concepts_id){
    this.hawbArray[0].customer_rate_id=this.rateListEdit.customer_rate_id;
        this.service.uploadexcel(this.hawbArray).subscribe(res=>{
          this.slap_edit_master(this.rateListEdit.customer_rate_id);
             this.reset_slap();
             this.toastr.successToastr('Successfully Added.', 'Success!'); 
             this.myInput.nativeElement.value="";
             this.excelLength=""
        });
    }

   downloadExcel(){
     // window.open("http://35.154.27.147:81/everest_express_billing_php/upload_template/TARIFF_EXCEL_DATA.xlsx");
   window.open("http://35.154.27.147:81/everest_express_billing_php/upload_template/TARIFF_EXCEL_DATA.xlsx");
   }

  ngOnInit() {
    this.getCountry();
    this.get_delivery_location();
  	this.branchList();
  	this.destination();
  	this.getPayment();
  	this.getService();
  	this.getShipment();
    this.getRate();
    this.slapList.slap_rate_type_UOM="KG";
  }

}
