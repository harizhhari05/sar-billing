
const Home = {
    text: 'Home',
    link: '/home',
    icon: 'icon-home'
};

const Dashboard = {
    text: 'Dashboard',
    link: '/dashboard/v1',
    icon: 'icon-speedometer',

};

const Masters = {
    text: 'Masters',
    link: '/masters',
    icon: 'icon-home'
};
const Bill_Generation = {
    text: 'Bill Generation',
    // link: '/dashboard/v1',
    icon: 'icon-note',
    submenu: [
        {
             text: 'Invoice Bill',
             link: '/invoice-gen',
        },
        {
            text: 'Duty Bill',
            link: '/duty-gen',
        },
        {
            text: 'Clearance Bill',
            link: '/clearance-gen',
        },
         {
            text: 'Service Bill',
            link: '/service-gen',
        }
    ]
};
const Report = {
    text: 'Reports',
    // link: '/dashboard/v1',
    icon: 'icon-graph',
    submenu: [
        {
             text: 'Invoice Bill',
             link: '/invoice-report',
        },
        {
            text: 'Duty Bill',
            link: '/duty-report',
        },
        {
            text: 'Non Invoice Bill',
            link: '/non-invoice-report',
        },
         {
            text: 'Non Duty Bill',
            link: '/non-duty-report',
        }
    ]
};
const Bill_view = {
    text: 'Bill View',
    // link: '/dashboard/v1',
    icon: 'icon-notebook',
    submenu: [
        {
             text: 'Duty Bill',
             link: '/duty-view',
        },
        {
            text: 'Invoice',
            link: '/invoice-view',
        },
        {
            text: 'Clearance Bill',
            link: '/clearance-view',
        },
        // {
        //     text: 'Air Freight Bill',
        //     link: '/clearance-view',
        // }
    ]
};

export const menu = [
  
    // Home,
    Dashboard,
    Masters,
    Bill_Generation,
    Bill_view,
    Report
   
];
