import { BillService } from '../../../services/bill.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invoice-issuing',
  templateUrl: './invoice-issuing.component.html',
  styleUrls: ['./invoice-issuing.component.scss']
})
export class InvoiceIssuingComponent implements OnInit {
	get_issue={
		point_id:"",
		starting_number:"",
		ending_number:""
	}
	issue_list:any;
	origin_list:any;
	length:any;
	tot_stock:any;

  constructor(public bill:BillService) { }

  get_Origin(){
  	this.bill.getBranch().subscribe(data=>{
  		this.origin_list=data['data'];
  		console.log("origin_list",this.origin_list)
  	})
  }

  get_Issue(){
  	this.bill.get_issue(this.get_issue).subscribe(data=>{
  		this.issue_list=data;
  		this.length=data['length'];
  		console.log("length",this.length);
  		this.tot_stock=0;
      	for (var i = 0; i < this.length; i++) {
        this.tot_stock = data[i]['Total_stock'] - this.tot_stock + this.tot_stock + this.tot_stock;
      	}
  		console.log("issue_list",this.tot_stock)
  		})
  }
  reset(){
  	this.get_issue.point_id="";
  	this.get_issue.starting_number="";
  	this.get_issue.ending_number="";
  	this.tot_stock=0;
  }
  export(){
  	console.log("y")
  	 var csvData = this.ConvertToCSV(this.issue_list);
        const blob: any = new Blob([csvData], { type: 'application/vnd.ms-excel' });

      let link = document.createElement("a");

      if (link.download !== undefined) {
        let url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        window.open(url)
        link.setAttribute("download", this.length);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
  }
  ConvertToCSV(objArray) {
            var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
              var str = '';
            var row = "";

            for (var index in objArray[0]) {
                //Now convert each value to string and comma-separated
                row += index + ',';
            }
            row = row.slice(0, -1);
            //append Label row with line break
            str += row + '\r\n';

            for (var i = 0; i < array.length; i++) {
                var line = '';
                for (var index in array[i]) {
                    if (line != '') line += ','

                    line += array[i][index];
                }
                str += line + '\r\n';
            }
            return str;
        }

  ngOnInit() {
  	this.get_Origin();
  	this.reset();
  	this.get_issue.starting_number="1000001";
  	this.tot_stock=0;
  }

}
