import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceIssuingComponent } from './invoice-issuing.component';

describe('InvoiceIssuingComponent', () => {
  let component: InvoiceIssuingComponent;
  let fixture: ComponentFixture<InvoiceIssuingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceIssuingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceIssuingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
