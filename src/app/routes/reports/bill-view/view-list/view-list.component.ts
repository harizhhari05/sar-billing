import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { BillService } from '../../../../services/bill.service';
import { Router,ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.scss']
})
export class ViewListComponent implements OnInit {
data3:any;
data2:any;
datas:any;
hawb_billingInvoice_no:any;
frieght_charges:any;
for_test:any;
tot_f_charge:any;
fuel_s_charge:any;
c_g_s_t:any;
s_g_s_t:any;
i_g_s_t:any;
tot_net_amount:any;
clien_name:any;
r_logo=false;
r_address=false;
pdf_bill_data={
  remove_logo:undefined,
  remove_address:undefined
}
invoice_no:any;
single_bill:any;
o_id:any;
c_id:any;
f_date:any;
t_date:any;

  constructor(public bill:BillService,private http:HttpClient,public route:ActivatedRoute,
   private _location: Location,public toastr: ToastrManager) { 
this.route.params.subscribe(data=>{
      this.hawb_billingInvoice_no=data['id'];
      this.clien_name=data['name'];
      this.o_id=data['o_id'];
      this.c_id=data['c_id'];
      this.f_date=data['f_date'];
      this.t_date=data['t_date'];
      console.log('id',this.hawb_billingInvoice_no)
    })
  }
  print_pdf(bill_data){
  this.pdf_bill_data = bill_data[0];

}
print() {
  console.log("pdf",this.pdf_bill_data);
      this.pdf_bill_data.remove_logo=this.r_logo;
      this.pdf_bill_data.remove_address=this.r_address;
      this.bill.getPdf_viewlist(this.pdf_bill_data).subscribe(params=>{
      this.data3=params;
console.log("list3",this.data3);
window.open(this.data3.file_url)
      })
    }

getlist(hawb_billingInvoice_no){
    this.bill.getList(this.hawb_billingInvoice_no).subscribe(params=>{
      this.data3=params['data'];
      this.frieght_charges=params['data']['length'];
      console.log("length",this.frieght_charges)
      this.tot_f_charge=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.tot_f_charge = params['data'][i]['freight_charge'] - this.tot_f_charge + this.tot_f_charge + this.tot_f_charge;
      }
      this.tot_f_charge=this.tot_f_charge.toFixed(2);
      this.fuel_s_charge=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.fuel_s_charge = params['data'][i]['FSC_charge'] - this.fuel_s_charge + this.fuel_s_charge + this.fuel_s_charge;
      }
      this.fuel_s_charge=this.fuel_s_charge.toFixed(2);
      this.c_g_s_t=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.c_g_s_t = params['data'][i]['CGST'] - this.c_g_s_t + this.c_g_s_t + this.c_g_s_t;
      }
      this.c_g_s_t=this.c_g_s_t.toFixed(2);
      this.s_g_s_t=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.s_g_s_t = params['data'][i]['SGST'] - this.s_g_s_t + this.s_g_s_t + this.s_g_s_t;
      }
      this.s_g_s_t=this.s_g_s_t.toFixed(2);
      this.i_g_s_t=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.i_g_s_t = params['data'][i]['IGST'] - this.i_g_s_t + this.i_g_s_t + this.i_g_s_t;
      }
      this.i_g_s_t=this.i_g_s_t.toFixed(2);
      this.tot_net_amount=0;
      for (var i = 0; i < this.frieght_charges; i++) {
        this.tot_net_amount = params['data'][i]['net_amount'] - this.tot_net_amount + this.tot_net_amount + this.tot_net_amount;
      }
      this.tot_net_amount=this.tot_net_amount.toFixed(2);

console.log("list3",this.data3);

  })
}
delete(id){
  this.bill.delete_invoice(id).subscribe(res=>{
    if (res[0]['CODE']==200) {
      this.toastr.successToastr('delete success','success');
      this.getlist(this.hawb_billingInvoice_no);
    }
    else{
      this.toastr.errorToastr('delete failed','failed');
    }
  })
}
back(){
  this._location.back();
}
addto(){
  this.bill.Addto(this.invoice_no).subscribe(res=>{
    if (res['status']=="success") {
      this.single_bill=res['data'];
      this.single_bill[0].InvoiceGenerateNumber=this.hawb_billingInvoice_no;
      this.bill.add_sinle_bill(this.single_bill).subscribe(f_res=>{
        if (f_res['code']==200) {
          this.toastr.successToastr('Add success','success');
          this.getlist(this.hawb_billingInvoice_no);
        }
        else{
          this.toastr.errorToastr('Add failed','failed');
        }
      })
    }
    else if(res['status']=="failed"){
      this.toastr.errorToastr('add failed','failed');
    }
  })
}
  ngOnInit() {
  	this.getlist(this.hawb_billingInvoice_no);
  }
}

