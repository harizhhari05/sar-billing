import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Router,ActivatedRoute } from '@angular/router';
import { BillService } from '../../../services/bill.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { Inject, Injectable } from '@angular/core';

@Component({
  selector: 'app-bill-view',
  templateUrl: './bill-view.component.html',
  styleUrls: ['./bill-view.component.scss'],
})
export class BillViewComponent implements OnInit {
data:any;
data1:any;
data2:any;
userid:any;
log_name:any;
data3:any;
datas={
  origin_id:undefined,
  origin_client_id:undefined,
  to_date:undefined,
  from_date:undefined
}
datas2:any;
r_logo=false;
r_address=false;
pdf_bill_data={
  remove_logo:undefined,
  remove_address:undefined
  }
  myDate = new Date();
  Date:any;
  o_id:any;
c_id:any;
f_date:any;
t_date:any;
edit_id:any;
btn:any;
edit_data={
  id:undefined,
  billing_date:undefined
}
  constructor(public bill:BillService,private http:HttpClient, private datePipe: DatePipe,
    public route:ActivatedRoute,public toastr: ToastrManager,
     @Inject(LOCAL_STORAGE)private storage: WebStorageService) {
  this.Date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
  this.route.params.subscribe(data=>{
      this.datas.origin_id=data['o_id'];
      this.datas.origin_client_id=data['c_id'];
      this.datas.from_date=data['f_date'];
      this.datas.to_date=data['t_date'];
      if (this.datas.to_date == "0000-00-00") {
      this.datas.to_date=this.Date;
    }
    if (this.datas.from_date == "0000-00-00") {
      this.datas.from_date=this.Date;
    }
      this.searchh();
    })
   }

   edit_date(data){
     this.edit_id=data.id;
     // this.edit_data.billing_date=this.data.billing_date;
   }
   update(){
     this.edit_data.id=this.edit_id;
     this.bill.upadte_date(this.edit_data).subscribe(res=>{
       if (res['code']==200) {
         this.toastr.successToastr('Date update success','success');
         this.searchh();
       }
     })
   }
    getbranch(){
  	this.bill.getBranch().subscribe(params=>{
      this.data=params['data'];
console.log("list",this.data);
  })
}
  getcus(origin_client_id){
  	this.bill.getCus(origin_client_id).subscribe(params=>{
      this.data1=params['data'];
console.log("list1",this.data1);

  })
}
print_prf(bill_data){
  this.pdf_bill_data = bill_data;

}
print() {
  console.log("pdf",this.pdf_bill_data);
      this.pdf_bill_data.remove_logo=this.r_logo;
      this.pdf_bill_data.remove_address=this.r_address;
      this.bill.getPdf(this.pdf_bill_data).subscribe(params=>{
      this.data3=params;
console.log("list3",this.data3);
window.open(this.data3.file_url)
      })
    }
  
  searchh(){
    if (this.datas.to_date == null || this.datas.to_date == undefined || this.datas.to_date =="") {
      this.datas.to_date=this.Date;
    }
    if (this.datas.from_date == null || this.datas.from_date == undefined || this.datas.from_date =="") {
      this.datas.from_date=this.Date;
    }
  	this.bill.getTab(this.datas).subscribe(params=>{
      this.data2=params['data'];
      if (this.data2.length==0 || this.data2.length==null) {
        this.toastr.warningToastr('No bills found','ALERT')
      }
  })
}
resett(){
	this.datas.origin_id='';
	this.datas.origin_client_id='';
    this.datas.to_date='';
    this.datas.from_date='';
    // this.searchh(this.datas);
}


  ngOnInit() {
    this.userid= this.storage.get("logdata");
    this.log_name=this.userid.user_name;
    if (this.log_name=="admin") {
      this.btn='1';
    }
    else{
      this.btn='2';
    }
    if (this.datas.to_date == null || this.datas.to_date == undefined || this.datas.to_date =="") {
      this.datas.to_date=this.Date;
    }
    if (this.datas.from_date == null || this.datas.from_date == undefined || this.datas.from_date =="") {
      this.datas.from_date=this.Date;
    }
    if (this.datas.origin_client_id == null || this.datas.origin_client_id == undefined || this.datas.origin_client_id =="") {
      this.datas.origin_client_id="";
    }
    if (this.datas.origin_id == null || this.datas.origin_id == undefined || this.datas.origin_id =="") {
      this.datas.origin_id="";
    }
    this.edit_data.billing_date=this.Date;
    // this.searchh();
  	this.getbranch();
    // this.resett();
  }
}