import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Router,ActivatedRoute } from '@angular/router';
import { BillService } from '../../../services/bill.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { Inject, Injectable } from '@angular/core';
import { PopupManager } from 'ng6-popup-boxes';
import {ExcelService} from '../../../excel.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-mothly-report',
  templateUrl: './mothly-report.component.html',
  styleUrls: ['./mothly-report.component.scss']
})
export class MothlyReportComponent implements OnInit {
	Report_list:any;
	insert={
		from_date:"",
		to_date:""
	}

  constructor(public bill:BillService,private http:HttpClient, private datePipe: DatePipe,
    public route:ActivatedRoute,public toastr: ToastrManager,
     @Inject(LOCAL_STORAGE)private storage: WebStorageService,private excelService:ExcelService,
     public popupManager: PopupManager) { }

  search(){
  	this.bill.get_month_report(this.insert).subscribe(data=>{
  		this.Report_list=data['data'];
  	})
  }
  export():void {
   	if (this.Report_list == null || this.Report_list == undefined || this.Report_list.length==0)
   	{
   		this.toastr.errorToastr('No Bill here!')
   	}else{
    this.excelService.exportAsExcelFile(this.Report_list, 'ALL_BILL');
}
 }

  ngOnInit() {
  }

}
