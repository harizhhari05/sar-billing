import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MothlyReportComponent } from './mothly-report.component';

describe('MothlyReportComponent', () => {
  let component: MothlyReportComponent;
  let fixture: ComponentFixture<MothlyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MothlyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MothlyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
