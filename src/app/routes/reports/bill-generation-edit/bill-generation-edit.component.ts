import { Component, OnInit } from '@angular/core';
import { BillService } from '../../../services/bill.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bill-generation-edit',
  templateUrl: './bill-generation-edit.component.html',
  styleUrls: ['./bill-generation-edit.component.scss']
})
export class BillGenerationEditComponent implements OnInit {
	Invoice_no:any;
	branchlist:any;
	paymentlist:any;
	serviceList:any;
	shipmentList:any;
	customerList:any;
	destinationList:any;
	bill_list={
		BOE_no: undefined,
		CGST: undefined,
		FSC_charge: undefined,
		GSTIN_No: undefined,
		GSTIN_Type: undefined,
		IGST: undefined,
		SGST: undefined,
		belongs_to_point_id: undefined,
		billing_status: undefined,
		chargable_weight: undefined,
		clearance_mode: undefined,
		created_by: undefined,
		created_by_name: undefined,
		created_time: undefined,
		currency_id: undefined,
		delivery_area: undefined,
		delivery_state_id: undefined,
		delivery_state_name: undefined,
		description_of_goods: undefined,
		destination_city: undefined,
		destination_country: undefined,
		destination_country_name: undefined,
		discount: undefined,
		document_type: undefined,
		document_type_name: undefined,
		duties_taxes: undefined,
		forwarder_no: undefined,
		freight_charge: undefined,
		freight_type: undefined,
		grand_total: undefined,
		hawb_booking_status: undefined,
		hold_reason: undefined,
		hold_status: undefined,
		iec_flag: undefined,
		igm_id: undefined,
		invoice_date: undefined,
		invoice_id: undefined,
		invoice_no: undefined,
		invoice_number: undefined,
		invoice_remarks: undefined,
		invoice_status: undefined,
		invoice_term: undefined,
		invoice_value:undefined,
		is_data_correction: undefined,
		is_form4_or_form5: undefined,
		kyc_verified_by: undefined,
		kyc_verified_status: undefined,
		manual_weight: undefined,
		net_amount: undefined,
		orgin_city: undefined,
		origin_client: undefined,
		origin_country: undefined,
		origin_country_name:undefined,
		origin_id: undefined,
		origin_name: undefined,
		other_charge1: undefined,
		other_charge2: undefined,
		parent_invoice: undefined,
		payment_mode: undefined,
		payment_status: undefined,
		pin_code: undefined,
		pod_verified_by: undefined,
		pod_verified_status:undefined,
		point_name: undefined,
		raw_invoice_id: undefined,
		receiver_IDproof: undefined,
		receiver_accNo: undefined,
		receiver_address: undefined,
		receiver_area: undefined,
		receiver_district_name: undefined,
		receiver_email: undefined,
		receiver_id: undefined,
		receiver_iecode: undefined,
		receiver_mobile: undefined,
		receiver_name: undefined,
		receiver_phone: undefined,
		receiver_state: undefined,
		reference_no: undefined,
		remarks:undefined,
		sender_accNo: undefined,
		sender_address: undefined,
		sender_area: undefined,
		sender_email: undefined,
		sender_id: undefined,
		sender_iqama_no: undefined,
		sender_mobile: undefined,
		sender_name: undefined,
		sender_phone_no: undefined,
		sender_pincode: undefined,
		sender_state: undefined,
		service: undefined,
		shipper: undefined,
		total_amount: undefined,
		total_carton: undefined,
		total_weight: undefined,
		transit_type_id: undefined,
		transit_type_name: undefined,
		trfZip_or_zipCode: undefined,
		updated_by: undefined,
		updated_by_name: undefined,
		updated_name: undefined,
		updated_time: undefined,
		vender: undefined,
		volumetric_weight: undefined,
		wh_storage_location_name: undefined,
	}

  constructor(public bill:BillService,public route:ActivatedRoute) {
  this.route.params.subscribe(data=>{
      this.Invoice_no=data['id'];
    })
   }
   branch_list(){
	  this.bill.getBranch().subscribe(params=>{
	  this.branchlist=params.data;
	  });
   }
   payment_list()
	{
	  this.bill.get_payment().subscribe(params=>{
	  this.paymentlist=params.data;
	  });
	}

   get_bill_list(){
   	this.bill.get_bill_details(this.Invoice_no).subscribe(data=>{
   		this.bill_list=data['data'];
   		console.log("data",this.bill_list);
   	})
   }
   getService(){
  	this.bill.get_service().subscribe(service=>{
  		this.serviceList=service['data'];
  	})
  }
  getShipment(){
  	this.bill.get_shipment_type().subscribe(shipment=>{
  		this.shipmentList=shipment['data'];
  	})
  }
  destination(){
  	this.bill.get_destination().subscribe(des=>{
  		this.destinationList=des['data'];
  	})
  }
  getcus(){
  	this.bill.getCus(-1).subscribe(params=>{
     this.customerList=params['data'];

  })
}

  ngOnInit() {
  	this.get_bill_list();
  	this.getService();
  	this.getShipment();
  	this.payment_list();
  	this.branch_list();
  	this.getcus();
  	this.destination();
  }

}
