import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillGenerationEditComponent } from './bill-generation-edit.component';

describe('BillGenerationEditComponent', () => {
  let component: BillGenerationEditComponent;
  let fixture: ComponentFixture<BillGenerationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillGenerationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillGenerationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
