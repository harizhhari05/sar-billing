import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Router } from "@angular/router";
import { ToastrManager } from 'ng6-toastr-notifications';
const swal = require('sweetalert');

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    valForm: FormGroup;
    passwordForm: FormGroup;
    stateList:any;
    branchList:any;
    boardList:any;
    mediumList:any;
    institudeList:any;
    city:any;
    state:any;
    insti:any;
    Board:any;
    Medium:any;
    send_otp={
        mobile_number:undefined,
        otp_num:undefined
    }
    verify_otp={
        ID:undefined,
        otp_num:undefined
    }
    otp:any;
    return_verify:any;
    cnfrm_verify:any;
    cnfrm_verify_1:any;

    constructor(public settings: SettingsService, fb: FormBuilder, private router: Router, private toast: ToastrManager) {

        let password = new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9!@#$%^&*()+]{6,10}$')]));
        let confirmPassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

        this.passwordForm = fb.group({
            'password': password,
            'confirmPassword': confirmPassword
        });

        this.valForm = fb.group({
            // 'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
            'name': [null, Validators.required],
            'number': [null, Validators.required],
            'altnumber': [null, Validators.required],
            // 'address': [null, Validators.required],
            'pin': [null, Validators.required],
            // 'user': [null, Validators.required],
            'passwordGroup': this.passwordForm,
            'password' : password,
        });
    }

    sweetalertDemo3() {
        swal("Register Completed!", "success");
    }

    // getMedium(){
    //     this.service.Get_medium().subscribe(Medium=>{
    //         this.mediumList=Medium['data'];
    //     })
    // }
    // reg(){
    //     for (let c in this.valForm.controls) {
    //         this.valForm.controls[c].markAsTouched();
    //     }
    //     for (let c in this.passwordForm.controls) {
    //         this.passwordForm.controls[c].markAsTouched();
    //     }

    //     if (this.valForm.valid) {
    //     console.log("val",this.valForm.value);
    //     this.router.navigate(['/login']);
    // }
    // }

    ngOnInit() {
        this.city="";
        this.state="";
        this.insti="";
        this.Board="";
        this.Medium="";
    }

}
