import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Router } from "@angular/router";
import { BillService } from '../../../services/bill.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import {ToasterModule, ToasterService, ToasterConfig} from 'angular2-toaster';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { Inject, Injectable } from '@angular/core';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
STORAGE_KEY = 'local_todolist';
     anotherTodolist = [];

    public data:any=[];
    valForm: FormGroup;
    logdata:any;
    userDetails : any;
    userCredential : Object;
    toaster: any;
    toasterConfig: any;
    toasterconfig: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-bottom-right',
        showCloseButton: true
    });
    constructor(public settings: SettingsService,
        public fb: FormBuilder,
        private router: Router,
        public toasterService: ToasterService,
        public toastr: ToastrManager,
        @Inject(LOCAL_STORAGE)private storage: WebStorageService,
        public service: BillService) {

        this.valForm = fb.group({
            'user': [null, Validators.required],
            'pass': [null, Validators.required]
        });

       this.toaster = {
            type: 'success',
            title: 'Title',
            text: 'Message'
        };

    }

    //  submitForm($ev, value: any) {
    //     if(value.password==this.userDetails.v_user_password){
    //            this.toasterMessage("success","Login","Successfully Login");
    //            this.router.navigate(['dashboard/v1']);
    //     }else {
    //         this.toasterMessage("error","Login","Failed to Login");
    //     }
    // }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {
            console.log('Valid!');
            console.log(value);
            this.service.getLogin(this.valForm.value).subscribe(res=>{
             console.log('res', res)
             this.userDetails=res['code'];
             console.log('det', this.userDetails)
             this.logdata=res['data']
             console.log('logd', this.logdata)

             this.getFromLocal(this.logdata);

             if(this.logdata['CODE']=='401')
             {
                 this.toasterMessage("error","Login","Incorrect username or password");
             }
             else {
                 console.log('reslogin ', res['data'])
                 this.storage.set("user_details",res['data']);
                 this.router.navigate(['dashboard']);
              this.toastr.successToastr('Login Success.', 'Success!');            
            }
         });
        }
        else{
            this.toasterMessage("error","Faild","Enter username or password");
        }
    }
        getLoginDetails(){
        console.log('val', this.valForm.value);
         this.service.getLogin(this.valForm.value).subscribe(res=>{
             console.log('res', res)
             this.userDetails=res['CODE'];
             console.log('det', this.userDetails)
             this.logdata=res['data']
             console.log('logd', this.logdata)

             this.getFromLocal(this.logdata);

             if(this.userDetails==200)
             {
                console.log('reslogin ', res['data'])
                this.storage.set("user_details",res['data']);
                this.router.navigate(['dashboard']);
                this.toastr.successToastr('Login Success.', 'Success!');
             }
             else {
            this.toastr.errorToastr('Invaild user', 'Faild!')          
            }
         });
             }

         toasterMessage(type,title,text) {
        this.toasterService.pop(type,title,text);
    };

getFromLocal(key): void {
    console.log('recieved= key:' + key);
    this.storage.set("logdata",key);
    // this.data[key]= this.storage.get(key);
    // console.log(this.data);

   }

    ngOnInit() {
        this.valForm = this.fb.group({
            'user': [null,],
            'pass': [null,]
        });

    }

}
