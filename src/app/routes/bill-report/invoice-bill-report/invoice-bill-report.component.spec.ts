import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceBillReportComponent } from './invoice-bill-report.component';

describe('InvoiceBillReportComponent', () => {
  let component: InvoiceBillReportComponent;
  let fixture: ComponentFixture<InvoiceBillReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceBillReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceBillReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
