import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Router,ActivatedRoute } from '@angular/router';
import { BillService } from '../../../services/bill.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { Inject, Injectable } from '@angular/core';
import {ExcelService} from '../../../excel.service';
import { PopupManager } from 'ng6-popup-boxes';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-invoice-bill-report',
  templateUrl: './invoice-bill-report.component.html',
  styleUrls: ['./invoice-bill-report.component.scss']
})
export class InvoiceBillReportComponent implements OnInit {
data:any;
data1:any;
data2:any;
userid:any;
log_name:any;
datas={
  origin_id:"",
  origin_client_id:"",
  to_date:undefined,
  from_date:undefined
}
  myDate = new Date();
  Date:any;
btn:any;
edit_data={
  id:undefined,
  billing_date:undefined
}
temp = [];
rowsFilter = [];
  constructor(public bill:BillService,private http:HttpClient, private datePipe: DatePipe,
    public route:ActivatedRoute,public toastr: ToastrManager,
     @Inject(LOCAL_STORAGE)private storage: WebStorageService,
     public popupManager: PopupManager,private excelService:ExcelService) {
  this.Date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
   }
    getbranch(){
  	this.bill.getBranch().subscribe(params=>{
      this.data=params['data'];
console.log("list",this.data);
  })
}
  getcus(origin_client_id){
  	this.bill.getCus(origin_client_id).subscribe(params=>{
      this.data1=params['data'];
console.log("list1",this.data1);

  })
}
export():void {
     if (this.rowsFilter == null || this.rowsFilter == undefined || this.rowsFilter.length==0)
     {
       this.toastr.errorToastr('No Bill here!')
     }else{
    this.excelService.exportAsExcelFile(this.rowsFilter, 'INVOICE BILL ');
}
 }
  updateFilter(event) {
   // this.searchh()
   // console.log(event)
        const val = event.target.value.toLowerCase();
// console.log(event.target.value)
        // filter our data

        const temp = this.data2.filter(function(data2) {
            return data2.hawb_billingInvoice_no.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rowsFilter = temp;
        // this.data2=this.rowsFilter;

        // this.table.offset = 0;
    }
  searchh(){
    if (this.datas.to_date == null || this.datas.to_date == undefined || this.datas.to_date =="") {
      this.datas.to_date=this.Date;
    }
    if (this.datas.from_date == null || this.datas.from_date == undefined || this.datas.from_date =="") {
      this.datas.from_date=this.Date;
    }
    if (this.datas.origin_client_id == null || this.datas.origin_client_id == undefined || this.datas.origin_client_id =="") {
      this.datas.origin_client_id="";
    }
    if (this.datas.origin_id == null || this.datas.origin_id == undefined || this.datas.origin_id =="") {
      this.datas.origin_id="";
    }
  	this.bill.get_invoice_report(this.datas).subscribe(params=>{
      this.data2=params['data'];
      this.rowsFilter=this.data2;
      console.log("report",this.data2)
      if (this.data2 == null || params['status'] == "failed") {
        this.toastr.warningToastr('No bills found','ALERT')
      }
  })
}
resett(){
	this.datas.origin_id='';
	this.datas.origin_client_id='';
}


  ngOnInit() {
    // this.userid= this.storage.get("logdata");
    // this.log_name=this.userid.user_name;
    // if (this.log_name=="admin") {
    //   this.btn='1';
    // }
    // else{
    //   this.btn='2';
    // }
    this.btn='1';
    if (this.datas.to_date == null || this.datas.to_date == undefined || this.datas.to_date =="") {
      this.datas.to_date=this.Date;
    }
    if (this.datas.from_date == null || this.datas.from_date == undefined || this.datas.from_date =="") {
      this.datas.from_date=this.Date;
    }
    if (this.datas.origin_client_id == null || this.datas.origin_client_id == undefined || this.datas.origin_client_id =="") {
      this.datas.origin_client_id="";
    }
    if (this.datas.origin_id == null || this.datas.origin_id == undefined || this.datas.origin_id =="") {
      this.datas.origin_id="";
    }
    this.edit_data.billing_date=this.Date;
    // this.searchh();
  	this.getbranch();
    // this.resett();
  }

}
