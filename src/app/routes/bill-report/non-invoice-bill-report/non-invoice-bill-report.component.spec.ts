import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonInvoiceBillReportComponent } from './non-invoice-bill-report.component';

describe('NonInvoiceBillReportComponent', () => {
  let component: NonInvoiceBillReportComponent;
  let fixture: ComponentFixture<NonInvoiceBillReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonInvoiceBillReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonInvoiceBillReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
