import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonDutyBillReportComponent } from './non-duty-bill-report.component';

describe('NonDutyBillReportComponent', () => {
  let component: NonDutyBillReportComponent;
  let fixture: ComponentFixture<NonDutyBillReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonDutyBillReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonDutyBillReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
