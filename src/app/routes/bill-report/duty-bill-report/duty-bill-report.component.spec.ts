import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DutyBillReportComponent } from './duty-bill-report.component';

describe('DutyBillReportComponent', () => {
  let component: DutyBillReportComponent;
  let fixture: ComponentFixture<DutyBillReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DutyBillReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DutyBillReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
