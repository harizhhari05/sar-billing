import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearanceBillViewComponent } from './clearance-bill-view.component';

describe('ClearanceBillViewComponent', () => {
  let component: ClearanceBillViewComponent;
  let fixture: ComponentFixture<ClearanceBillViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearanceBillViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearanceBillViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
