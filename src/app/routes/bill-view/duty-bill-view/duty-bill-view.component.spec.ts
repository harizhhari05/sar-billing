import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DutyBillViewComponent } from './duty-bill-view.component';

describe('DutyBillViewComponent', () => {
  let component: DutyBillViewComponent;
  let fixture: ComponentFixture<DutyBillViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DutyBillViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DutyBillViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
