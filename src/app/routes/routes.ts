import { LayoutComponent } from '../layout/layout.component';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { LockComponent } from './pages/lock/lock.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';
import { MastersComponent } from './masters/masters.component';
import { BillGenerationComponent } from './reports/bill-generation/bill-generation.component'
import { BillViewComponent } from './reports/bill-view/bill-view.component';
import { InvoiceIssuingComponent } from './reports/invoice-issuing/invoice-issuing.component';
import { WithoutSaleEntryComponent } from './reports/without-sale-entry/without-sale-entry.component';
import { ViewListComponent } from './reports/bill-view/view-list/view-list.component';
import { BillGenerationEditComponent } from './reports/bill-generation-edit/bill-generation-edit.component';
import { DutyBillComponent } from './bill-generation/duty-bill/duty-bill.component';
import { ClearanceBillComponent } from './bill-generation/clearance-bill/clearance-bill.component';
import { ServiceBillComponent } from './bill-generation/service-bill/service-bill.component';
import { InvoiceBillComponent } from './bill-view/invoice-bill/invoice-bill.component';
import { InvoiceBillGenComponent } from './bill-generation/invoice-bill-gen/invoice-bill-gen.component';
import { ClearanceBillViewComponent } from './bill-view/clearance-bill-view/clearance-bill-view.component';
import { DutyBillViewComponent } from './bill-view/duty-bill-view/duty-bill-view.component';
import { InvoiceBillReportComponent } from './bill-report/invoice-bill-report/invoice-bill-report.component';
import { DutyBillReportComponent } from './bill-report/duty-bill-report/duty-bill-report.component';
import { NonDutyBillReportComponent } from './bill-report/non-duty-bill-report/non-duty-bill-report.component';
import { NonInvoiceBillReportComponent } from './bill-report/non-invoice-bill-report/non-invoice-bill-report.component';
import { MothlyReportComponent } from './reports/mothly-report/mothly-report.component';
import { AirFightComponent } from './bill-generation/air-fight/air-fight.component';
import { AirFreightComponent } from './bill-view/air-freight/air-freight.component';

export const routes = [
{ path: '', 
    redirectTo: 'login', 
    pathMatch: 'full' 
    },

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'masters', component:MastersComponent },
            { path: 'bill-generation', component:BillGenerationComponent },
            { path: 'bill-view/:o_id/:c_id/:f_date/:t_date', component:BillViewComponent},
            { path: 'invoice-issuing', component:InvoiceIssuingComponent},
            { path: 'without-sale-entry', component:WithoutSaleEntryComponent},
            { path: 'view-list/:id/:name/:o_id/:c_id/:f_date/:t_date', component:ViewListComponent },
            { path: 'bill-edit/:id', component:BillGenerationEditComponent },
            { path: 'duty-gen', component:DutyBillComponent },
            { path: 'clearance-gen', component:ClearanceBillComponent },
            { path: 'invoice-view', component:InvoiceBillComponent},
            { path: 'invoice-gen', component:InvoiceBillGenComponent},
            { path: 'clearance-view', component:ClearanceBillViewComponent},
            { path: 'duty-view', component:DutyBillViewComponent },
            { path: 'service-gen', component:ServiceBillComponent },
            { path: 'invoice-report', component:InvoiceBillReportComponent },
            { path: 'duty-report', component:DutyBillReportComponent },
            { path: 'non-invoice-report', component:NonInvoiceBillReportComponent },
            { path: 'non-duty-report', component:NonDutyBillReportComponent },
            { path: 'mothly_report', component:MothlyReportComponent },
            { path: 'air-gen', component:AirFightComponent },
            { path: 'air-view', component:AirFreightComponent }
            
        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'recover', component: RecoverComponent },
    { path: 'lock', component: LockComponent },
    { path: 'maintenance', component: MaintenanceComponent },
    { path: '404', component: Error404Component },
    { path: '500', component: Error500Component },

    // Not found
    { path: '**', redirectTo: 'login' }

];
