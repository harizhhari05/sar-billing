import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslatorService } from '../core/translator/translator.service';
import { MenuService } from '../core/menu/menu.service';
import { SharedModule } from '../shared/shared.module';
import { PagesModule } from './pages/pages.module';

import { menu } from './menu';
import { routes } from './routes';
import { MastersComponent } from './masters/masters.component';
import { BillGenerationComponent } from './reports/bill-generation/bill-generation.component';
import { BillViewComponent } from './reports/bill-view/bill-view.component';
import { InvoiceIssuingComponent } from './reports/invoice-issuing/invoice-issuing.component';
import { WithoutSaleEntryComponent } from './reports/without-sale-entry/without-sale-entry.component';
import { ViewListComponent } from './reports/bill-view/view-list/view-list.component';
import { BillGenerationEditComponent } from './reports/bill-generation-edit/bill-generation-edit.component';
import { DutyBillComponent } from './bill-generation/duty-bill/duty-bill.component';
import { ClearanceBillComponent } from './bill-generation/clearance-bill/clearance-bill.component';
import { ServiceBillComponent } from './bill-generation/service-bill/service-bill.component';
import { InvoiceBillComponent } from './bill-view/invoice-bill/invoice-bill.component';
import { InvoiceBillGenComponent } from './bill-generation/invoice-bill-gen/invoice-bill-gen.component';
import { ClearanceBillViewComponent } from './bill-view/clearance-bill-view/clearance-bill-view.component';
import { DutyBillViewComponent } from './bill-view/duty-bill-view/duty-bill-view.component';
import { InvoiceBillReportComponent } from './bill-report/invoice-bill-report/invoice-bill-report.component';
import { DutyBillReportComponent } from './bill-report/duty-bill-report/duty-bill-report.component';
import { NonDutyBillReportComponent } from './bill-report/non-duty-bill-report/non-duty-bill-report.component';
import { NonInvoiceBillReportComponent } from './bill-report/non-invoice-bill-report/non-invoice-bill-report.component';
import { MothlyReportComponent } from './reports/mothly-report/mothly-report.component';
import { AirFightComponent } from './bill-generation/air-fight/air-fight.component';
import { AirFreightComponent } from './bill-view/air-freight/air-freight.component';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forRoot(routes, { useHash: true }),
        PagesModule
    ],
    declarations: [MastersComponent, BillGenerationComponent, BillViewComponent, InvoiceIssuingComponent, WithoutSaleEntryComponent, ViewListComponent, BillGenerationEditComponent, DutyBillComponent, ClearanceBillComponent, ServiceBillComponent, InvoiceBillComponent, InvoiceBillGenComponent, ClearanceBillViewComponent, DutyBillViewComponent, InvoiceBillReportComponent, DutyBillReportComponent, NonDutyBillReportComponent, NonInvoiceBillReportComponent, MothlyReportComponent, AirFightComponent, AirFreightComponent],
    exports: [
        RouterModule
    ]
})

export class RoutesModule {
    constructor(public menuService: MenuService, tr: TranslatorService) {
        menuService.addMenu(menu);
    }
}
